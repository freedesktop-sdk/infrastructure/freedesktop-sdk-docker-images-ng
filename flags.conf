variables:
  debug_flags: "-g"
  common_flags: "-O2 %{debug_flags} -pipe"
  target_common_flags: "%{common_flags} -Wp,-D_FORTIFY_SOURCE=2 -Wp,-D_GLIBCXX_ASSERTIONS -fexceptions -fstack-protector-strong -grecord-gcc-switches"
  target_flags_x86_64: "%{target_common_flags} -fasynchronous-unwind-tables -fstack-clash-protection -fcf-protection"
  # -mstackrealign is to  a potential but unlikely bug introduced
  # by the change of the Linux i386 ABI a long time ago. Stack used to be
  # 4-bytes aligned, now it is 16-bytes aligned for SSE. Not available
  # by default. So it is passed in flags.
  # See https://gcc.gnu.org/bugzilla/show_bug.cgi?id=40838
  target_flags_i686: "-mstackrealign %{target_common_flags} -fasynchronous-unwind-tables -fstack-clash-protection -fcf-protection"
  target_flags_aarch64: "%{target_common_flags} -fasynchronous-unwind-tables -fstack-clash-protection"
  target_flags_arm: "%{target_common_flags}"
  ldflags_defaults: "-Wl,-z,relro,-z,now -Wl,--as-needed"

environment:
  (?):
    - target_arch == "x86_64":
        CFLAGS:  "%{target_flags_x86_64}"
        CXXFLAGS: "%{target_flags_x86_64}"
        LDFLAGS:  "%{ldflags_defaults}"
    - target_arch == "i686":
        CFLAGS: "%{target_flags_i686}"
        CXXFLAGS: "%{target_flags_i686}"
        LDFLAGS:  "%{ldflags_defaults}"
    - target_arch == "arm":
        CFLAGS:  "%{target_flags_arm}"
        CXXFLAGS: "%{target_flags_arm}"
        LDFLAGS:  "%{ldflags_defaults}"
    - target_arch == "aarch64":
        CFLAGS:  "%{target_flags_aarch64}"
        CXXFLAGS: "%{target_flags_aarch64}"
        LDFLAGS:  "%{ldflags_defaults}"
